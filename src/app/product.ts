export class Product {

    id:number;
	  name:string;
	  price:number;
      categoryId:number;
      imageUrl:string;

      constructor(id,name,
        price,
        categoryId,
        imageUrl){
            this.id=id;
            this.name=name;
            this.price=price;
            this.categoryId=categoryId;
            this.imageUrl=imageUrl;


      }
}
