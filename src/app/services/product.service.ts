import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Product } from '../product';
import {  HttpClient } from '@angular/common/http';

const apiUrl= 'http://localhost:9000/products';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

 


  constructor(private http:HttpClient) {
    
   }

  getProducts(): Observable<Product[]>{
    return  this.http.get<Product[]>(apiUrl);
 
  }
}
